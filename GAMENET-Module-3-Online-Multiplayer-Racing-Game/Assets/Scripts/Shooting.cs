﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject LastHitFrom;

    [Header ("Shooting Variables")]
    [SerializeField] bool isProjectileShooter;
    [SerializeField] float fireDelay = 0;
    [SerializeField] GameObject FirePoint;
    public float Damage = 20;
    public bool canShoot = false;

    [Header ("Health Variables")]
    public float startHealth = 100;
    public Image Healthbar;
    bool isDead = false;
    [SerializeField] private float health;

    // Start is called before the first frame update
    void Start()
    {
        photonView.RPC("SetHealth", RpcTarget.All);
        camera = transform.Find("Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Space key was pressed.");
            Fire();
        }
    }
    
    public void Fire() {
        if (canShoot) {
            if (!isProjectileShooter) {
                RaycastHit hit;
                Ray ray = camera.ViewportPointToRay(new Vector3 (0.5f,0.6f));
        
                if (Physics.Raycast(ray, out hit, 1000)) {
                    if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) {
                    hit.collider.gameObject.GetComponent<Shooting>().UpdateLastHit(this.gameObject);
                    hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, Damage);
                    Debug.Log("Raycast Hit" );
                    }
                }
                
            }

            else if (isProjectileShooter) {
                // Instantiate Projectile
                GameObject Bullet = PhotonNetwork.Instantiate("BulletProjectile", FirePoint.transform.position, FirePoint.transform.rotation);
                Bullet.GetComponent<Projectile>().BulletDamage = Damage;
                Bullet.GetComponent<Projectile>().Shooter = this.gameObject;
                Debug.Log("Projectile Fired");
            }

            canShoot = false;
            StartCoroutine(ShootingDelay());
        }
    }

    [PunRPC]
    public void TakeDamage(float damage) {
        if (isDead == false) {
            if (health > 0) {
            this.health -= damage;
            this.Healthbar.fillAmount = health / startHealth;
            }

            if (health <= 0) {
                photonView.RPC("Die", RpcTarget.All);
            }

            
        }
    }
    [PunRPC]
    public void SetHealth() {
        health = startHealth;
    }

    [PunRPC]
    public void Die() {
        if (isDead == false) {
        isDead = true;
        this.GetComponent<LastManController>().PlayerDeathEvent();
        }
        
    }

    public void UpdateLastHit(GameObject lasthitfrom) {
            LastHitFrom = lasthitfrom;
    }

    IEnumerator ShootingDelay() {
        if (!canShoot) {
            yield return new WaitForSeconds(fireDelay);
            canShoot = true;
        }
    }
}
