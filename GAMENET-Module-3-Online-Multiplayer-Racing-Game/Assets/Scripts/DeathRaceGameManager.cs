﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class DeathRaceGameManager : MonoBehaviour
{
    public GameObject[] vehiclePrefabs;
    public Transform[] StartingPosition;
    public GameObject[] finisherTextsUi;
    public GameObject GameOverCanvas;
    public GameObject VictoryCanvas;
    public int playerCount;

    public static DeathRaceGameManager instance = null;
    public Text timeText;

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        GameOverCanvas.SetActive(false);
        VictoryCanvas.SetActive(false);
        
        playerCount = PhotonNetwork.CountOfPlayers;

        if (PhotonNetwork.IsConnectedAndReady) {
            object playerSelectionNumber;
            if(PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)) {
                Debug.Log((int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = StartingPosition[actorNumber - 1].position;
                PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
        }

        foreach (GameObject go in finisherTextsUi) {
            go.SetActive(false);
        }
    }

    public void GameOver() {
        GameOverCanvas.SetActive(true);
    }

     public void Victory() {
        VictoryCanvas.SetActive(true);
    }

    public void OnLeaveButton() {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("LobbyScene");
    }
}
