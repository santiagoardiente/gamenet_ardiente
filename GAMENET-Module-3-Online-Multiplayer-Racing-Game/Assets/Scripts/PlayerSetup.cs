﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;


public class PlayerSetup : MonoBehaviourPunCallbacks
{   
    public TMP_Text Nameplate;

    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {   
        Nameplate.text = photonView.Owner.NickName;
        camera = transform.Find("Camera").GetComponent<Camera>();
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc")) {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine;
            GetComponent<LastManController>().enabled = false;
            GetComponent<Shooting>().enabled = false;
            GetComponent<CountdownManager>().timerText = RacingGameManager.instance.timeText;
            Debug.Log("Racing Stats Loaded");
            camera.enabled = photonView.IsMine;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr")) {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = false;
            GetComponent<LastManController>().enabled = photonView.IsMine;
            GetComponent<Shooting>().enabled = photonView.IsMine;
            GetComponent<CountdownManager>().timerText = DeathRaceGameManager.instance.timeText;
            Debug.Log("Death Race Stats Loaded");
            camera.enabled = photonView.IsMine;
        }
    }


}
