﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class LastManController : MonoBehaviourPunCallbacks
{

    public enum RaiseEventsCode {
        WhoDiedEventCode = 0
    }

    public int deathOrder = 0;

    private void OnEnable() {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable() {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoDiedEventCode) {
            object[] data = (object[]) photonEvent.CustomData;
            
            string nickNameOfFinishedPlayer = (string) data[0];
            deathOrder = (int)data[1];
            int viewId = (int) data[2];

            GameObject orderUiText = DeathRaceGameManager.instance.finisherTextsUi[deathOrder - 1];

            orderUiText.SetActive(true);
            
            if (viewId == photonView.ViewID) { // check if u

                orderUiText.GetComponent<Text>().text = nickNameOfFinishedPlayer + " died.  (YOU) " + DeathRaceGameManager.instance.playerCount + " players are left.";
                orderUiText.GetComponent<Text>().color = Color.red;
                DeathRaceGameManager.instance.GameOver();
            }
            else {
                orderUiText.GetComponent<Text>().text = nickNameOfFinishedPlayer + " died. " + DeathRaceGameManager.instance.playerCount + " players are left.";

                if (DeathRaceGameManager.instance.playerCount <= 1) {
                    DeathRaceGameManager.instance.Victory();
                }
            }
        }
    }

       // Start is called before the first frame update
    void Start()
    {

    }

    public void PlayerDeathEvent() {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false;
        GetComponent<Shooting>().enabled = false;

        DeathRaceGameManager.instance.playerCount--;
        deathOrder++;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;
        // Event Data
        object[] data = new object[]{nickName, deathOrder, viewId}; 

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };
        SendOptions sendOption = new SendOptions {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoDiedEventCode, data, raiseEventOptions,sendOption );
    }

    void Update() {
        
    }
}
