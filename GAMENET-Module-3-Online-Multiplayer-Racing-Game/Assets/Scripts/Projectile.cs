﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Projectile : MonoBehaviourPunCallbacks
{
    public float BulletDamage;
    public float Duration;
    public GameObject Shooter;

    void Start() {
        StartCoroutine(BulletDuration());
    }

    void Update() {
       BulletTranslation();
    }
    
    public void OnTriggerEnter(Collider collider) {
        if (collider.GetComponent<Shooting>() && collider.GetComponent<PhotonView>().IsMine) {
            collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, BulletDamage);
            collider.gameObject.GetComponent<Shooting>().UpdateLastHit(Shooter);
        }
        Destroy(this.gameObject);
    }

    public void BulletTranslation() {
        this.transform.Translate(Vector3.forward * Time.deltaTime * 30);
    }

    IEnumerator BulletDuration() {
        yield return new WaitForSeconds(Duration);
        Destroy(this.gameObject);
    }
}
