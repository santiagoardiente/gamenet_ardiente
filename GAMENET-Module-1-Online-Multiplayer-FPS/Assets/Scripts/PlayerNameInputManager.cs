﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class PlayerNameInputManager : MonoBehaviour
{
    public void SetPlayerName(string name) {
        if (string.IsNullOrEmpty(name)) {
            Debug.LogWarning("Player name is Empty");
            return;
        }
        PhotonNetwork.NickName = name;
    }
}
