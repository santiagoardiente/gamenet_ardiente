﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
public class TakingDamage : MonoBehaviourPunCallbacks
{
    private float startHealth = 100;
    public float Health;
    
    [SerializeField] 
    Image healthbar;
    void Start() {
        Health = startHealth;
        healthbar.fillAmount = Health / startHealth;
    }
       [PunRPC] 
       public void takeDamage(int damage) {
        Health -= damage;
        Debug.Log(Health);

        healthbar.fillAmount = Health / startHealth;
        if (Health <= 0) {
            Die();
        }
    }
    private void Die() {
        if (photonView.IsMine) {
            GameManager.Instance.LeaveRoom();
        }
    }
}
