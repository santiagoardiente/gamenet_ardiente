﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;
    public GameObject GlobalUI;
    public GameObject KillMessagePrefab;
    public static GameManager Instance;
    public GameObject[] SpawnPoints;
    public GameObject AnnouncementBoard;
    bool hasWon = false;
    private void Awake() {
        if (Instance = null) {
            Destroy(this.gameObject);
        }
        else {
            Instance = this;
        }
    }

    void Start() {
        int randomSpawnPointIndex = Random.Range(0,SpawnPoints.Length);

        if ( PhotonNetwork.IsConnectedAndReady) {
        PhotonNetwork.Instantiate(playerPrefab.name, SpawnPoints[randomSpawnPointIndex].transform.position, Quaternion.identity);
        }
    }
    void updateAnnouncementBoard(string TextToDisplay) {
        AnnouncementBoard.transform.Find("AnnouncementText").GetComponent<Text>().text = TextToDisplay;
        StartCoroutine(AnnouncementBoardFade());
    }

    public void AddToKillFeed(string Killer, string Victim) {
        GameObject KillMessageGameObject = Instantiate(KillMessagePrefab);
        KillMessageGameObject.transform.SetParent(GlobalUI.transform.Find("KillFeed").transform,false);
        KillMessageGameObject.transform.Find("KillMessage").GetComponent<Text>().text = Killer + " killed " + Victim;
        Destroy(KillMessageGameObject, 5.0f);
    }
    IEnumerator AnnouncementBoardFade() {
        yield return new WaitForSeconds(5.0f);
        AnnouncementBoard.transform.Find("AnnouncementText").GetComponent<Text>().text = " ";
    }
    [PunRPC]
    public void GameOver(string Winner) {
        if (hasWon == false) {
            hasWon = true;
            string winningText = Winner + " has won the game.";
            updateAnnouncementBoard(winningText);
        }
    }

}