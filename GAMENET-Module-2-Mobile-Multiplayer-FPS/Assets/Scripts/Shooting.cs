﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public int Kills;
    public GameObject hitEffectPrefab;
    public GameObject LastHitFrom;

    [Header ("HP Related Stuff")]
    public float startHealth = 100;
    public Image Healthbar;
    bool isDead = false;

    private Animator animator;
    private float health;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
        health = startHealth;
        Healthbar.fillAmount = health / startHealth;
    }

    public void Fire() {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3 (0.5f,0.5f));
        
        if (Physics.Raycast(ray, out hit, 200)) {
            photonView.RPC("CreateHitEffects",RpcTarget.All, hit.point);

            
            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine) {
                hit.collider.gameObject.GetComponent<Shooting>().UpdateLastHit(this.gameObject);
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info) {
        if (isDead == false) {
            if (health > 0) {
            this.health -= damage;
            this.Healthbar.fillAmount = health / startHealth;
            }

            if (health <= 0 && isDead == false) {
                photonView.RPC("Die", RpcTarget.AllBuffered);
                GameManager.Instance.AddToKillFeed(info.Sender.NickName,info.photonView.Owner.NickName);
                AddKilltoKiller();
            }
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position) {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
            Destroy(hitEffectGameObject, 0.2f);
    }

    // [PunRPC]
    public void AddKilltoKiller() {
        LastHitFrom.GetComponent<Shooting>().Kills++;
        LastHitFrom.GetComponent<Shooting>().UpdateKillHud();
    }
    public void UpdateLastHit(GameObject lasthitfrom) {
            LastHitFrom = lasthitfrom;
    }

    public void UpdateKillHud() {
        GameObject.Find("Kills").GetComponent<Text>().text = "Kills: " + this.GetComponent<Shooting>().Kills.ToString();  
    }
    [PunRPC]
    public void Die() {
        if (photonView.IsMine) {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountDown());
        }
        isDead = true;
    }

    IEnumerator RespawnCountDown() {
        GameObject respawnText = GameObject.Find("RespawnText");
        float respawnTime = 5.0f;
        while (respawnTime > 0) {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            this.GetComponent<PlayerSetup>().fpsModel.SetActive(false);
            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You have been killed. Respawning in " + respawnTime.ToString(".00");
        }
        animator.SetBool("isDead", false);
        this.GetComponent<PlayerSetup>().fpsModel.SetActive(true);
        respawnText.GetComponent<Text>().text = " ";

        this.transform.position = GameManager.Instance.SpawnPoints[Random.Range(0,GameManager.Instance.SpawnPoints.Length)].transform.position;
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }
    [PunRPC]
    public void RegainHealth() {
        health = startHealth;
        Healthbar.fillAmount = health / startHealth;
        isDead = false;
    }

    void Update() {
        // UpdateKillHud();
        if(Kills  == 10) {
            Debug.Log("You have won");
            GameManager.Instance.GetComponent<PhotonView>().RPC("GameOver", RpcTarget.All, photonView.Owner.NickName);
        }
    }
}
