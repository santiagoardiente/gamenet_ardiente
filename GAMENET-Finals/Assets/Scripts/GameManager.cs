﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance = null;
    public GameObject GlobalUI;

    public GameObject[] finisherTextsUi;
    public GameObject[] PlayerPrefabs;
    public Transform[] StartingPosition;
    public GameObject GameOverCanvas;
    public GameObject VictoryCanvas;

    public GameObject Camera;
    public int playerCount;

    public TMP_Text timeText;

    
    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start() {  
        GameOverCanvas.SetActive(false);
        VictoryCanvas.SetActive(false);
        
        playerCount = PhotonNetwork.CountOfPlayers;

        if (PhotonNetwork.IsConnectedAndReady) {
            object playerSelectionNumber;
            if(PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber)) {
                Debug.Log("Player Selection Number: " + (int)playerSelectionNumber);

                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = StartingPosition[actorNumber - 1].position;
                PhotonNetwork.Instantiate(PlayerPrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
                Debug.Log ("Player Instantiated");
            }
        }
    }
    public void Victory() {
        VictoryCanvas.SetActive(true);
    }
    public void GameOver() {
        GameOverCanvas.SetActive(true);
    }

    public void OnExitButtonClick() {
        PhotonNetwork.LoadLevel(0);
        PhotonNetwork.LeaveRoom();
        
    }
}
