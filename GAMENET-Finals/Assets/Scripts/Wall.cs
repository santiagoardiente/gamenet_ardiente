﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Wall : MonoBehaviour
{
    float wallDuration;
    float wallHealth;
    
    void Start() {
        Destroy(this.gameObject, wallDuration);
    }
    [PunRPC]
    public void InitializeWall(float duration, float health) {
        wallDuration = duration;
        wallHealth = health;
    }

    [PunRPC]
    public void TakeDamage( float value) {
        wallHealth -= value;

        if (wallHealth < 0) {
            Destroy(this.gameObject);
        } 
    }
}
