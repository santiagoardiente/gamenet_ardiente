﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class Unit : MonoBehaviourPunCallbacks
{
    
    [Header("Skill Based")]
    [SerializeField] float healPotency;
    [SerializeField] float buildPotency;
    public float weaponPotency;
    public float healCooldown;
    public float buildCooldown;
    public bool canBuild = true;
    public bool canHeal = true;
    public bool isActivated = false;
    public bool isDead = false;
    public Transform BuildPoint;
    public Gun currentWeapon;
    public GameObject WallPrefab;

    [Header("Health Based")]
    public float maxHealth;
    public float currentHealth;

    [Header("UI Based")]
    public PlayerInterface PlayerUI;
    public Camera Camera;
    
    #region Unity Functions
    void Update(){
        Movement();
        LookAtMouse();
        Combat();
    }

    void Start() {
        photonView.RPC("SetHealth", RpcTarget.AllBuffered);
        currentWeapon.Holder = this;
        currentWeapon.canShoot = true;
    }
    #endregion

    #region Keybind Functions

    void Combat() {
        if (!isActivated) return;

        if (Input.GetMouseButton(0)) {
            currentWeapon.Fire();
        }

        if (Input.GetKeyDown("r"))
        {
            currentWeapon.Reload();
        }

        if (Input.GetKeyDown("q"))
        {
            BuildWall();
        }
        

        if (Input.GetKeyDown("e"))
        {
            HealSkill();
        }
    }

    void Movement() {
        if (isActivated) {
            float Xtranslation = Input.GetAxis("Horizontal");
            float Ytranslation = Input.GetAxis("Vertical");

            Vector3 Movement = new Vector3(Xtranslation, Ytranslation, 0);

            this.transform.position += (Movement * 15 * Time.deltaTime);
        }
    }

    void LookAtMouse() {
        Vector2 mousePos = Camera.ScreenToWorldPoint(Input.mousePosition);
        Vector2 lookDir = mousePos - GetComponent<Rigidbody2D>().position;
        float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;
        GetComponent<Rigidbody2D>().rotation = angle;
    }

    #endregion

    #region Skill Based Functions
    public void BuildWall() {
        if (canBuild) {
            canBuild = false;
            GameObject Wall = PhotonNetwork.Instantiate("WallPrefab", BuildPoint.transform.position, BuildPoint.transform.rotation);
            Wall.GetComponent<PhotonView>().RPC("InitializeWall", RpcTarget.AllBuffered, buildPotency, 10 * buildPotency);
            PlayerUI.BuildCooldown = buildCooldown;
            StartCoroutine(BuildSkillCD());
        }
    }

    public void HealSkill() {
        if (currentHealth >= maxHealth) return;

        if (canHeal) {
            canHeal = false;
            float HealStrength = 15 * (healPotency / 10);
            photonView.RPC("GainHealth", RpcTarget.AllBuffered, HealStrength);
            PlayerUI.HealCooldown = healCooldown;
            StartCoroutine(HealSkillCD());
        }
    }

    IEnumerator BuildSkillCD() {
        yield return new WaitForSeconds(buildCooldown);
        canBuild = true;
    }

    IEnumerator HealSkillCD() {
        yield return new WaitForSeconds(healCooldown);
        canHeal = true;
    }
    
    #endregion

    #region Health Based Functions

    [PunRPC]
    void SetHealth() {
        currentHealth = maxHealth;
    }

    [PunRPC]
    public void GainHealth(float value) {
        currentHealth += value;
        if (currentHealth > maxHealth) {
            currentHealth = maxHealth;
        }
    }

    [PunRPC]
    public void TakeDamage(float value) {
        if (isDead) return;
        currentHealth -= value;
        if (currentHealth <= 0) {
            currentHealth = 0; // set to flat 0
            photonView.RPC("Death", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    public void Death() {
        if (!isDead) {
            isDead = true;
            GetComponent<GameController>().PlayerDeathEvent();
        }
        
    }

    #endregion
}
