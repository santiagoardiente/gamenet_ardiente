﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Projectile : MonoBehaviourPunCallbacks
{
    public float BulletDamage;
    public float Duration;
    public GameObject Shooter;

    void Start() {
        StartCoroutine(BulletDuration());
    }

    void Update() {
       BulletTranslation();
    }
    
    public void OnTriggerEnter2D(Collider2D collider) {
        if (collider.GetComponent<Unit>() && collider.GetComponent<PhotonView>().IsMine) {
            collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, BulletDamage);
            Debug.Log ("PLAYER HIT");
        }
        else if (collider.GetComponent<Wall>() && collider.GetComponent<PhotonView>().IsMine){
            collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, BulletDamage);
            Debug.Log ("WALL HIT");
        }

        Destroy(this.gameObject);
    }

    public void BulletTranslation() {
        this.transform.Translate(Vector3.up * Time.deltaTime * 200);
    }

    IEnumerator BulletDuration() {
        yield return new WaitForSeconds(Duration);
        Destroy(this.gameObject);
    }
}
