﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class GameController : MonoBehaviourPunCallbacks
{

    public enum RaiseEventsCode {
        WhoDiedEventCode = 0
    }

    public int deathOrder = 0;

    private void OnEnable() {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable() {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent) {
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoDiedEventCode) {
            object[] data = (object[]) photonEvent.CustomData;
            
            string nickNameOfFinishedPlayer = (string) data[0];
            deathOrder = (int)data[1];
            int viewId = (int) data[2];
            
            if (viewId == photonView.ViewID) { // check if u
                GameManager.instance.GameOver();
            }
            else {
                if (GameManager.instance.playerCount <= 1 || deathOrder == 1) {
                    GameManager.instance.Victory();
                }
            }
        }
    }

    public void PlayerDeathEvent() {
        GetComponent<Unit>().enabled = false;

        GameManager.instance.playerCount--;
        deathOrder++;

        string nickName = photonView.Owner.NickName;
        int viewId = photonView.ViewID;
        // Event Data
        object[] data = new object[]{nickName, deathOrder, viewId}; 

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };
        SendOptions sendOption = new SendOptions {
            Reliability = false
        };
        PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoDiedEventCode, data, raiseEventOptions,sendOption );
    }
}
