﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerInterface : MonoBehaviour
{
    public TMP_Text HealCoolDownText;
    public TMP_Text BuildCoolDownText;
    public TMP_Text CurrentMagText;
    public TMP_Text MagazineSizeText;
    public TMP_Text isReloadingText;
    public Image Healthbar;

    public Unit PlayerReference;
    
    public float BuildCooldown = 0;
    public float HealCooldown = 0;

    void Update() {
        Healthbar.fillAmount = PlayerReference.currentHealth / PlayerReference.maxHealth;
        CurrentMagText.text = PlayerReference.currentWeapon.currentMag.ToString();
        MagazineSizeText.text = PlayerReference.currentWeapon.magazineSize.ToString();

        if (PlayerReference.currentWeapon.isReloading) {
            isReloadingText.text = "RELOADING!";
        }
        else if(!PlayerReference.currentWeapon.isReloading) {
            isReloadingText.text = " ";
        }

        // Build CD
        if(BuildCooldown > 0) {
            StartBuildCD();
        }

        else if (BuildCooldown <= 0)
            BuildCoolDownText.text = "RDY";

        // Heal CD
        if(HealCooldown > 0) {
            StartHealCD();
        }

        else if (HealCooldown <= 0)
            HealCoolDownText.text = "RDY";
    
    }

    public void StartHealCD() {
        HealCooldown -= Time.deltaTime;
        HealCoolDownText.text = HealCooldown.ToString("F0"); 
    }

    public void StartBuildCD() {
        BuildCooldown -= Time.deltaTime;
        BuildCoolDownText.text = BuildCooldown.ToString("F0"); 
    }
}
