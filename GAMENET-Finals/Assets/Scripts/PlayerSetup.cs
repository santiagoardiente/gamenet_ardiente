﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;


public class PlayerSetup : MonoBehaviourPunCallbacks
{   
    public GameObject PlayerUIPrefab;
    public GameObject PlayerCameraPrefab;
    
    void Start()
    {   
        GetComponent<Unit>().enabled = photonView.IsMine;
        GetComponent<GameController>().enabled = photonView.IsMine;
        
        if (photonView.IsMine) {
            GameObject playerUI = Instantiate(PlayerUIPrefab);
            playerUI.GetComponent<PlayerInterface>().PlayerReference = this.GetComponent<Unit>();
            GetComponent<Unit>().PlayerUI = playerUI.GetComponent<PlayerInterface>();

            GameObject playerCamera = Instantiate(PlayerCameraPrefab);
            GetComponent<Unit>().Camera = playerCamera.GetComponent<Camera>();;
            playerCamera.GetComponent<CameraFollow>().target = this.transform;
        }
        GetComponent<CountdownManager>().timerText = GameManager.instance.timeText;

        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("Desert")) {
            Debug.Log("Desert Map Loaded");
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("Plains")) {
            
            Debug.Log("Plains Map Loaded");
        }
        
    }


}
