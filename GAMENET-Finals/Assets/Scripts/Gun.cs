﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Gun : MonoBehaviour
{
    public float reloadSpeed;
    public float gunDamage;
    public float gunFireRate;
    public float magazineSize;
    public float currentMag;

    public GameObject FirePoint;
    public Unit Holder;

    public bool canShoot = true;
    public bool isReloading = false;

    public void Fire() {
        if (!canShoot) return;
        if (isReloading) return;

        if (currentMag > 0) {
            PhotonNetwork.Instantiate("BulletPrefab", FirePoint.transform.position, FirePoint.transform.rotation);
            StartCoroutine(FireDelay());
            currentMag--;
        }
        else
        {
            if (!isReloading) {
                Reload();
            }
        }
    }

    public void Reload() {
        StartCoroutine(ReloadTime());
    }

    IEnumerator ReloadTime() {
        isReloading = true;
        canShoot = false;
        yield return new WaitForSeconds(reloadSpeed - reloadSpeed * (Holder.weaponPotency / 100) );
        currentMag = magazineSize;
        canShoot = true;
        isReloading = false;
    }

    IEnumerator FireDelay() {
        if (canShoot) {
            canShoot = false;
            yield return new WaitForSeconds(gunFireRate - gunFireRate * (Holder.weaponPotency / 200));
            canShoot = true;
        }
    }


}
