﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class CountdownManager : MonoBehaviourPunCallbacks {
    public TMP_Text timerText;
    public float timeToStartFight = 5f;

    void Update() {
        if (PhotonNetwork.IsMasterClient) {
            if (timeToStartFight > 0) {
                timeToStartFight -= Time.deltaTime;
                photonView.RPC("SetTime", RpcTarget.AllBuffered, timeToStartFight);
            }
            else if (timeToStartFight < 0) {
                photonView.RPC("StartMatch", RpcTarget.AllBuffered);
            }
        }
    }

    [PunRPC]
    public void SetTime(float time) {
        if (time > 0) {
            timerText.text = time.ToString("F1");
        }
        else {
            timerText.text = " ";
        }
    }

    [PunRPC]
    public void StartMatch() {
        GetComponent<Unit>().isActivated = true;
        this.enabled = false;
    }
}